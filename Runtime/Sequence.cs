﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

namespace com.FDT.Sequences
{
    [System.Serializable]
    public class Sequence
    {
        [System.Serializable]
        public class UnityEventAsync:UnityEvent<Sequence>
        {}

        [SerializeField] protected List<SequenceItem> _items = new List<SequenceItem>();
        [SerializeField] protected UnityEvent _onFinishEvt;

        private CancellationTokenSource _cancelToken;
        private bool waitingCompletion = false;
        private bool _isPlaying = false;
        public bool IsPlaying { get => _isPlaying; }

        public async void Run()
        {
            if (_isPlaying)
            {
                Debug.LogWarning($"Sequence {this} already playing");
                return;
            }

            _isPlaying = true;
            _cancelToken = new CancellationTokenSource();
            float cTime;
            float finishTime;
            for (int i = 0; i < _items.Count; i++)
            {
                _items[i].isCurrent = false;
                _items[i].waitAfterProgress = 0;
                _items[i].waitBeforeProgress = 0;
                _items[i].isWaitingAfter = false;
                _items[i].isWaitingBefore = false;
                _items[i].isExecuting = false;
            }

            for (int i = 0; i < _items.Count; i++)
            {
                if (!_items[i].enabled) continue;

                _items[i].isCurrent = true;
                if (_items[i].waitBefore > 0)
                {
                    _items[i].isWaitingBefore = true;
                    //Debug.Log($"waiting {_items[i].waitBefore} before");
                    cTime = Time.time;
                    finishTime = cTime + _items[i].waitBefore;
                    do
                    {
                        cTime+= (_items[i].realtime?Time.unscaledDeltaTime:Time.deltaTime);
                        _items[i].waitBeforeProgress = (finishTime - cTime) / _items[i].waitBefore;
                        await Task.Yield();
                    } while (cTime < finishTime && !_cancelToken.IsCancellationRequested);

                    if (_cancelToken.IsCancellationRequested)
                        return;
                    _items[i].isWaitingBefore = false;
                }

                if (_items[i].async)
                {
                    _items[i].isExecuting = true;
                    //Debug.Log($"executing async {i}");
                    waitingCompletion = true;
                    _items[i].actionAsync.Invoke(this);

                    do
                    {
                        await Task.Yield();
                    } while (waitingCompletion && !_cancelToken.IsCancellationRequested);
                        
                    if (_cancelToken.IsCancellationRequested)
                        return;
                    _items[i].isExecuting = false;
                }
                else
                {
                    //Debug.Log($"executing {_items[i].description}");
                    _items[i].action.Invoke();
                }

                if (_items[i].waitAfter > 0)
                {
                    _items[i].isWaitingAfter = true;
                    //Debug.Log($"waiting {_items[i].waitAfter} before");
                    cTime = Time.time;
                    finishTime = cTime + _items[i].waitAfter;
                    do
                    {
                        cTime+= (_items[i].realtime?Time.unscaledDeltaTime:Time.deltaTime);
                        _items[i].waitAfterProgress = (finishTime - cTime) / _items[i].waitAfter;
                        await Task.Yield();
                    } while (cTime < finishTime && !_cancelToken.IsCancellationRequested);

                    if (_cancelToken.IsCancellationRequested)
                        return;
                    
                    _items[i].isWaitingAfter = true;
                }
                _items[i].isCurrent = false;
            }
            //Debug.Log("finished sequence");
            _onFinishEvt.Invoke();
            _isPlaying = false;
        }

        public void ItemComplete()
        {
            //Debug.Log("received complete");
            waitingCompletion = false;
        }

        public void OnDestroy()
        {
            if (_cancelToken!= null)
                _cancelToken.Cancel();
        }

        public void AddItem(SequenceItem item)
        {
            _items.Add(item);
        }
        public void AddItem(UnityAction method1, string description, float waitAfter = 1, float waitBefore = 1, bool realtime = false, Color color = default)
        {
            var item = SequenceItem.Create(method1, description, waitAfter, waitBefore, realtime, color);
            _items.Add(item);
        }
        public void AddAsyncItem(UnityAction<Sequence> method1, string description, float waitAfter = 1, float waitBefore = 1, bool realtime = false, Color color = default)
        {
            var item = SequenceItem.CreateAsync(method1, description, waitAfter, waitBefore, realtime, color);
            _items.Add(item);
        }
    }
}

