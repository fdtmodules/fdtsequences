﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace com.FDT.Sequences
{
    [System.Serializable]
    public class SequenceItem
    {
        
        public bool enabled;
        [TextArea(3, 6)] public string description;
        public Color color;
        public float waitBefore;
        public float waitAfter;
        public bool realtime;
        public UnityEvent action;
        public bool async;
        public Sequence.UnityEventAsync actionAsync;
        [NonSerialized] public bool isCurrent;
        [NonSerialized] public float waitBeforeProgress;
        [NonSerialized] public float waitAfterProgress;
        [NonSerialized] public bool isWaitingAfter;
        [NonSerialized] public bool isWaitingBefore;
        [NonSerialized] public bool isExecuting;

        public static SequenceItem Create(UnityAction method1, string description, float waitAfter = 1, 
            float waitBefore = 1, bool realtime = false, Color color = default)
        {
            SequenceItem item = Create(description, waitAfter, waitBefore, realtime, color);
            item.async = false;
            item.action = new UnityEvent();
            item.action.AddListener(method1);
            return item;
        }

        public static SequenceItem CreateAsync(UnityAction<Sequence> method1, string description, float waitAfter = 1, 
            float waitBefore = 1, bool realtime = false, Color color = default)
        {
            SequenceItem item = Create(description, waitAfter, waitBefore, realtime, color);
            item.async = true;
            item.actionAsync = new Sequence.UnityEventAsync();
            item.actionAsync.AddListener(method1);
            return item;
        }

        private static SequenceItem Create(string description, float waitAfter,
            float waitBefore, bool realtime, Color color)
        {
            SequenceItem item = new SequenceItem();
            item.enabled = true;
            item.description = description;
            item.waitAfter = waitAfter;
            item.waitBefore = waitBefore;
            item.realtime = realtime;
            item.color = color;
            return item;
        }
    }
}