# FDT Sequences

Graphic coroutine wrapper to design sequences in editor or by code


## Usage

The package must be included using the manifest.json file in the project.
The file must be modified to include this dependencies:

```json
{
  "dependencies": {
	"com.fdt.sequences": "https://gitlab.com/fdtmodules/fdtsequences.git#2021.1.0",
	"com.fdt.common": "https://gitlab.com/fdtmodules/fdtcommon.git#2021.1.0",

	...
  }
}

```

## License

MIT - see [LICENSE](https://gitlab.com/fdtmodules/fdtsequences/src/2021.1.0/LICENSE.md)